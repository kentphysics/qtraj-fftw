// ============================================================
//  Jump parameters
// ============================================================

// do jumps?
doJumps 	0

// max jumps
maxJumps	10

// ============================================================
//  Simulation parameters
// ============================================================

// number of quantum trajectories to simulate
nTrajectories	1

// random seed to use; can be set to a fix long integer; if it is 0 then time is used
randomseed	0

// maximum initial random #
rMax	1

// ============================================================
//  Potential parameters
// ============================================================

// potential to use
//
//  0  Munich potential
//  1  Isotropic KSU potential
//  2  Anisotropic KSU potential
//
potential	0

// ============================================================
//  Physics parameters
// ============================================================

// the reduced mass in GeV
m	2.365

// coulomb coupling
alpha   0.6239853

// kappa value to use; numerical value must be >=0
// if -1 use central fit, if -2 use lower fit, if -3 use upper fit
//
kappa	-1

// gamma value to use (default is -1.75)
gam	-1.75

// ============================================================
//  Temperature parameters
// ============================================================

// temperature evolution type
//
//	0	Ideal Bjorken evolution using T0 and Tf below
// 	1	Read from file; if this option is chosen then temperatureFile needs to be set appropriately below
//	2	Read from "trajectory file" with filename given by temperatureFile below
//
temperatureEvolution	0

// initial temperature in GeV when using Bjorken temperature evolution
T0	0.425

// final temperature in GeV; must be lower than Tf
Tf	0.25

// temperature file
temperatureFile ./input/temperature/trajectory_0_180.txt

// initial time for vacuum evolution in 1/GeV
// 0.6 fm/c = 3.0406390839551767
t0	0

// time to turn on hydro background in 1/GeV
// can turn off medium by setting to large number
tmed	3.0406390839551767

// ============================================================
//  Initial condition parameters
// ============================================================

// initial condition to use
//
//  0		Singlet Coulomb; respects the n and l settings below (default)
//  1		Gaussian delta function
//  100 	Computed eigenstates; respects the n and l settings below; not supported in lightweight mode
//  200 	Load eigenstates from file; respects the n and l settings below; file is set using basisFunctionsFile below
//
initType	1

// projection type
//
//  0 		Use Coulomb eigenstates
//  1		Use computed eigenstates; not supported in lightweight mode
//  2		Use disk-based eigenstates; file is set using basisFunctionsFile below
//
projType	0

// basis functions file
basisFunctionsFile	./input/basisfunctions_ksu_4096.tsv

// init width for Gaussian IC in units of a_0; form exp(-r/(c a_0)^2)
initWidth	0.2

// params for initial condition, n, l, and color type (0=singlet, 1=octet)
initN		1
initL		0
initC		0


// ============================================================
//  Grid parameters
// ============================================================

// number of grid points; SHOULD BE A POWER OF 2 for best performance
num	4096

// length of the simulation box in 1/GeV
L	80

// time step in 1/GeV
dt		0.001

// maximum number of time steps
maxSteps	40000

// derivative type 
//      0 = k^2/2m (full derivative)
//      1 = (1-cos(k*dr))/m/dr/dr (second-order differences)
/
derivType	0

// ============================================================
//  Output parameters
// ============================================================

// summary output and snapshot frequency
snapFreq	1000

// snapshot points; should be <= num
snapPts		1024

// flag for whether to add randomseed to end of output directory name
//   0 = output directory is "output"
//   1 = output directory is "output-<seed>"
//
dirnameWithSeed		0

// turn on/off saving of wavefunctions
saveWavefunctions       1

// turn on/off output of summary.tsv file
outputSummaryFile	0

