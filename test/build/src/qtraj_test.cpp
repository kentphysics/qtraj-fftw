/*
 
 s1r_test.cpp
 
 Copyright (c) Michael Strickland and Sabin Thapa
 
 GNU General Public License (GPLv3)
 See detailed text in license directory
 
 */

#include <gtest/gtest.h>
#include <fstream>
#include <iostream>
#include <sstream>

#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <vector>

#include "trajectory.h"
#include "paramreader.h"
#include "wavefunction.h"
#include "outputroutines.h"

using namespace std;

class row {
public:
    double data[4];
};

void loadWavefunction(string fileName, vector<row> *dataVector) {
    ifstream inFile;
    inFile.open(fileName);
    string line;
    while (getline(inFile, line)) {
        istringstream ss(line);
        row myData;
        ss >> myData.data[0] >> myData.data[1] >> myData.data[2] >> myData.data[3];
        dataVector->push_back(myData);
    }
    inFile.close();
}

TEST(EvolutionTest, NormAndEigenstates_Munich) {

    readParametersFromFile("input/params_NormTest_Munich.txt",1);

    for (int state=0; state<nBasis; state++) {

        int row = floor(-0.5 + sqrt(0.25 + 2 * state));
        int triangularNumber = row * (row + 1) / 2;
        int column = state - triangularNumber;

        initN = row+1;
        initL = column;

        cout << "Checking state " << state << " --> initN = " << initN << " and initL = " << initL << endl;
        processParameters();

        print_line(); // cosmetic

        initializeEvolution();

        double norm = evolveWavefunction(0,maxSteps);

        // compute observables
        //double norm = computeNorm(wfnc);
        double diff = 0;
        for (int i=0; i<num; i++)  diff += abs(wfnc[i]) - abs(basisFunctions[state*num+i]);
        diff /= num;

        // free memory
        finalizeEvolution();

        // do checks
        ASSERT_NEAR(1, norm, 1.0e-6);
        ASSERT_NEAR(0, diff, 1.0e-6);
    }

}


TEST(EvolutionTest, NormAndEigenstates_KSU) {

    readParametersFromFile("input/params_NormTest_KSU.txt",1);

    for (int state=0; state<nBasis; state++) {

        int row = floor(-0.5 + sqrt(0.25 + 2 * state));
        int triangularNumber = row * (row + 1) / 2;
        int column = state - triangularNumber;

        initN = row+1;
        initL = column;

        cout << "Checking state " << state << " --> initN = " << initN << " and initL = " << initL << endl;
        processParameters();

        print_line(); // cosmetic

        initializeEvolution();

        double norm = evolveWavefunction(0,maxSteps);

        // compute observables
        // double norm = computeNorm(wfnc);
        double diff = 0;
        for (int i=0; i<num; i++)  diff += abs(wfnc[i]) - abs(basisFunctions[state*num+i]);
        diff /= num;

        // free memory
        finalizeEvolution();

        // do checks
        ASSERT_NEAR(1, norm, 1.0e-6);
        ASSERT_NEAR(0, diff, 1.0e-6);
    }

}

TEST(EvolutionTest, GaussianNoJumpTest) {

    // read in the target data into data structure
    vector<row> target_Data;
    loadWavefunction("input/targets/target_Gaussian_NoJump.tsv",&target_Data);

    // run simulation
    readParametersFromFile("input/params_Gaussian_NoJump.txt",1);
    processParameters();
    initializeEvolution();
    double norm = evolveWavefunction(0,maxSteps);
    finalizeEvolution();
    print_line();
    cout << "Complete" << endl;
    print_line();

    // read in the test data into data structure
    vector<row> test_Data;
    loadWavefunction("output/snapshot_14940.tsv",&test_Data);

    // compute "distance" between two data files
    double diff = 0;
    for (int i=0; i<target_Data.size(); i++)
        for (int j=0; j<4; j++)
            diff += fabs(target_Data[i].data[j] - test_Data[i].data[j]);
    diff /= target_Data.size();
    
    // should be close to zero
    ASSERT_NEAR(diff, 0, 1e-8);
}


TEST(EvolutionTest, GaussianNoJumpPwaveTest) {

    // read in the target data into data structure
    vector<row> target_Data;
    loadWavefunction("input/targets/target_Gaussian_NoJump_pwave.tsv",&target_Data);

    // run simulation
    readParametersFromFile("input/params_Gaussian_NoJump_pwave.txt",1);
    processParameters();
    initializeEvolution();
    double norm = evolveWavefunction(0,maxSteps);
    finalizeEvolution();
    print_line();
    cout << "Complete" << endl;
    print_line();

    // read in the test data into data structure
    vector<row> test_Data;
    loadWavefunction("output/snapshot_14940.tsv",&test_Data);

    // compute "distance" between two data files
    double diff = 0;
    for (int i=0; i<target_Data.size(); i++)
        for (int j=0; j<4; j++)
            diff += fabs(target_Data[i].data[j] - test_Data[i].data[j]);
    diff /= target_Data.size();
    
    // should be close to zero
    ASSERT_NEAR(diff, 0, 1e-8);
}

TEST(EvolutionTest, GaussianJumpTest) {

    // read in the target data into data structure
    vector<row> target_Data;
    loadWavefunction("input/targets/target_Gaussian_Jump.tsv",&target_Data);

    // run simulation
    readParametersFromFile("input/params_Gaussian_Jump.txt",1);
    processParameters();
    initializeEvolution();

    print_line();
    cout << "==> Classical evolution" << endl;
    print_line();

    // perform vacuum evolution (tau = 0 -> tmed)
    double myNorm = evolveWavefunction(0,nmed);
    for (int i=0;i<num;i++) wfncSave[i] = wfnc[i]; // save result of vacuum evolution
    // do rest of the vacuum evolution to fix myNorm
    myNorm = evolveWavefunction(nmed,maxSteps-nmed);
    saveNoJumpsSingletOverlaps();

    print_line();
    cout << "==> Heff final norm is " << myNorm << endl;
    print_line();

    for (int i=0;i<num;i++) wfnc[i] = wfncSave[i]; // load wfnc at t=tmed

    print_line();
    cout << " Quantum evolution" << endl;
    print_line();

    evolveWavefunctionWithJumps(nmed,myNorm);

    // perform output
    outputRatios(&metadata,nBasis,singletOverlaps,singletOverlaps0);

    finalizeEvolution();

    print_line();
    cout << "==> Complete" << endl;
    print_line();
    
    // read in the test data into data structure
    vector<row> test_Data;
    loadWavefunction("output/snapshot_3550.tsv",&test_Data);

    // compute "distance" between two data files
    double diff = 0;
    for (int i=0; i<target_Data.size(); i++)
        for (int j=0; j<4; j++)
            diff += fabs(target_Data[i].data[j] - test_Data[i].data[j]);
    diff /= target_Data.size();
    
    // should be close to zero
    ASSERT_NEAR(diff, 0, 1e-8);
}


TEST(EvolutionTest, GaussianJumpPwaveTest) {

    // read in the target data into data structure
    vector<row> target_Data;
    loadWavefunction("input/targets/target_Gaussian_Jump_pwave.tsv",&target_Data);

    // run simulation
    readParametersFromFile("input/params_Gaussian_Jump_pwave.txt",1);
    processParameters();
    initializeEvolution();

    print_line();
    cout << "==> Classical evolution" << endl;
    print_line();

    // perform vacuum evolution (tau = 0 -> tmed)
    double myNorm = evolveWavefunction(0,nmed);
    for (int i=0;i<num;i++) wfncSave[i] = wfnc[i]; // save result of vacuum evolution
    // do rest of the vacuum evolution to fix myNorm
    myNorm = evolveWavefunction(nmed,maxSteps-nmed);
    saveNoJumpsSingletOverlaps();

    print_line();
    cout << "==> Heff final norm is " << myNorm << endl;
    print_line();

    for (int i=0;i<num;i++) wfnc[i] = wfncSave[i]; // load wfnc at t=tmed

    print_line();
    cout << " Quantum evolution" << endl;
    print_line();

    evolveWavefunctionWithJumps(nmed,myNorm);

    // perform output
    outputRatios(&metadata,nBasis,singletOverlaps,singletOverlaps0);

    finalizeEvolution();

    print_line();
    cout << "==> Complete" << endl;
    print_line();
    
    // read in the test data into data structure
    vector<row> test_Data;
    loadWavefunction("output/snapshot_3342.tsv",&test_Data);

    // compute "distance" between two data files
    double diff = 0;
    for (int i=0; i<target_Data.size(); i++)
        for (int j=0; j<4; j++)
            diff += fabs(target_Data[i].data[j] - test_Data[i].data[j]);
    diff /= target_Data.size();
    
    // should be close to zero
    ASSERT_NEAR(diff, 0, 1e-8);
}

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

