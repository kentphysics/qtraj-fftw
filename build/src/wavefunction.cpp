/*
 
 wavefunction.cpp
 
 Copyright (c) Michael Strickland
 
 GNU General Public License (GPLv3)
 See detailed text in license directory
 
 */

#include <iostream>
#include <cstdlib>
#include <cstdio>
#include <cmath>
#include <stdio.h>

using namespace std;

#include "potential.h"
#include "initialcondition.h"

// Computes Exp(-I x dt)
inline dcomp kernelFunc(dcomp x) {
    // multiply by -I dt and exponentiate
    return exp(dcomp(imag(x)*dt,-real(x)*dt));
}

// compute norm
double computeNorm(dcomp* wfnc)
{
    double N = 0;
    for (int i=0;i<num;i++) N += real(conj(wfnc[i])*wfnc[i]);
    return N*dx;
}

void normalizeWavefunction(dcomp* wfnc) {
    double N = computeNorm(wfnc);
    N = 1/sqrt(N);
    for (int i=0;i<num;i++) wfnc[i] *= N;
}

// compute <r>/s0
double computeRexp(dcomp* wfnc, double norm)
{
    double rexp = 0;
    for (int i=0;i<num;i++) rexp += real(conj(wfnc[i])*wfnc[i])*(i+1);
    return rexp*dx*dx*(alpha*m*2./3.)/norm; // scale by 1s <r>
}

// setup the space kernel
void loadSpaceKernel(double T, double ax, double ay, double az, double Lam)
{
    //for (int i=0;i<num;i++) spaceKernel[i] = kernelFunc(0.5*Veff((i+1)*dx,T));
    for (int i=0;i<num;i++) spaceKernel[i] = kernelFunc(0.5*Veff((i+1)*dx,T,ax,ay,az,Lam));
}

// setup the mom kernel and load to device
void loadMomKernel()
{
    for (int i=0;i<num;i++)  {
        const double k = (i+1)*dk;
        if (derivType==0) momKernel[i] = kernelFunc(dcomp(0.5*k*k/m,0));
        else momKernel[i] = kernelFunc(dcomp((1-cos(k*dx))/m/dx/dx,0));
    }
}

// setup intial wave function
void loadInitialWavefunction()
{
    int initState = 0.5*initN*(initN-1)+initL;
    if (initType==100 && initState>nBasis-1) { cout << "initState not in basis elements loaded.  Increase nBasis." << endl; exit(-1); }
    for (int i=0;i<num;i++) {
        wfnc[i] = basisFunctions[initState*num+i];
        if (initType<100)
            wfnc[i] = initialWavefunction((i+1)*dx, initN, initL);
        else
            wfnc[i] = basisFunctions[initState*num+i];
    }
    // normalize wavefunction
    normalizeWavefunction(wfnc);
    // save for convenient access later
    for (int i=0;i<num;i++) wfncInit[i] = wfnc[i];
}

inline void DST(fftw_plan p, dcomp* wfnc, double* in, double* out)
{
    // load real part
    for (int i=0;i<num;i++) in[i] = real(wfnc[i]);
    // DST
    fftw_execute(p);
    // save real part in real part of wfnc
    for (int i=0;i<num;i++) wfnc[i] = dcomp(out[i]/sqrt(2*(num + 1)),imag(wfnc[i]));
    // load imaginary part
    for (int i=0;i<num;i++) in[i] = imag(wfnc[i]);
    // DST
    fftw_execute(p);
    // save imag part in the imag part of wfnc
    for (int i=0;i<num;i++) wfnc[i] = dcomp(real(wfnc[i]),out[i]/sqrt(2*(num + 1)));
    return;
}

void makeStep(fftw_plan p, dcomp* wfnc, double* in, double* out, dcomp* spaceKernel, dcomp* momKernel)
{
    // make one spatial half-step
    for (int i=0; i<num; i++) wfnc[i] = spaceKernel[i]*wfnc[i];
    // forward DST transform
    DST(p, wfnc, in, out);
    // make one full step in momentum space
    for (int i=0; i<num; i++) wfnc[i] = momKernel[i]*wfnc[i];
    // backward DST transform; it's its own inverse
    DST(p, wfnc, in, out);
    // make one spatial half-step
    for (int i=0; i<num; i++) wfnc[i] = spaceKernel[i]*wfnc[i];
}
