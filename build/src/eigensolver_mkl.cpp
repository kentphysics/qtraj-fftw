/*
 
 eigensolver.cpp
 
 Copyright (c) Michael Strickland
 
 GNU General Public License (GPLv3)
 See detailed text in license directory
 
 */

#include <cstdlib>
#include <cstdio>
#include <iostream>
#include "math.h"
#include "mkl_lapacke.h"

#include "eigensolver.h"
#include "outputroutines.h"
#include "potential.h"

using namespace std;

void computeStatesMKL(double T, double l, double *d, double *z) {
    
    MKL_INT info;
    
    double *e = (double *)malloc(sizeof(double)*(num-1)); // off diagonals
    
    const double mass = 2*m; // mass from reduced mass
    double lapfac = 1/mass/dx/dx;
    
    for (int i=0;i<num;i++) {
        double r = (i+1)*dx;
        d[i] = 2*lapfac + 2*mass + real(V(r,T)) + l*(l+1)/mass/r/r;
    }
    for (int i=0;i<num-1;i++) e[i]=-lapfac;
    
    info = LAPACKE_dsteqr(LAPACK_ROW_MAJOR, 'I', num, d, e, z, num);
    
    if( info > 0 ) {
        printf( "Eigensolver failed to converge.  Exiting.\n" );
        exit(-1);
    }
    
    free(e);
}

inline void extractEigenvector(double* eigenvecs, dcomp* mycol, int col) {
    for (int i=0; i<num; i++)
        mycol[i] = dcomp(eigenvecs[i*num+col]/sqrt(dx),0); // load taking into account different normalization
}

void loadBasisStates(dcomp *basisFunctions, double T) {
    
    cout << "==> Finding all eigenstates (MKL)" << endl;
    
    // l = 0, 1, 2
    double *vals0 = (double *)malloc(sizeof(double)*num); // eigenvalues
    double *vecs0 = (double *)malloc(sizeof(double)*num*num); // eigenvectors
    double *vals1 = (double *)malloc(sizeof(double)*num); // eigenvalues
    double *vecs1 = (double *)malloc(sizeof(double)*num*num); // eigenvectors
    double *vals2 = (double *)malloc(sizeof(double)*num); // eigenvalues
    double *vecs2 = (double *)malloc(sizeof(double)*num*num); // eigenvectors
    
    computeStatesMKL(T,0,vals0,vecs0);
    computeStatesMKL(T,1,vals1,vecs1);
    computeStatesMKL(T,2,vals2,vecs2);
    
    print_line();
    cout << "==> M(1s) = " << vals0[0] << endl;
    cout << "==> M(2s) = " << vals0[1] << endl;
    cout << "==> M(2p) = " << vals1[0] << endl;
    cout << "==> M(3s) = " << vals0[2] << endl;
    cout << "==> M(3p) = " << vals1[1] << endl;
    cout << "==> M(3d) = " << vals2[0] << endl;
    print_line();
    
    extractEigenvector(vecs0, &basisFunctions[0*num], 0);
    extractEigenvector(vecs0, &basisFunctions[1*num], 1);
    extractEigenvector(vecs1, &basisFunctions[2*num], 0);
    extractEigenvector(vecs0, &basisFunctions[3*num], 2);
    extractEigenvector(vecs1, &basisFunctions[4*num], 1);
    extractEigenvector(vecs2, &basisFunctions[5*num], 0);
    
    //    for (int i=0;i<num;i++) {
    //        cout << (i+1)*dx << "\t" << basisFunctions[i].x << endl;
    //    }
    
    free(vals0);
    free(vecs0);
    free(vals1);
    free(vecs1);
    free(vals2);
    free(vecs2);
}
