/*
 
 wavefunction.h
 
 Copyright (c) Michael Strickland
 
 GNU General Public License (GPLv3)
 See detailed text in license directory
 
*/

#ifndef __wavefunction_h__
#define __wavefunction_h__

#include <complex>
#include <fftw3.h>

typedef std::complex<double> dcomp;

double computeNorm(dcomp* wfnc);

void normalizeWavefunction(dcomp* wfnc);
double computeRexp(dcomp* wfnc, double norm);

void loadSpaceKernel(double T, double ax, double ay, double az, double Lam);
void loadMomKernel();
void loadInitialWavefunction();

void makeStep(fftw_plan p, dcomp* wfnc, double* in, double* out, dcomp* spaceKernel, dcomp* momKernel);

#endif /* __wavefunction_h__ */
