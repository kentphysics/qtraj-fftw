/*

   initialcondition.h

   Copyright (c) Michael Strickland

   GNU General Public License (GPLv3)
   See detailed text in license directory

*/

#ifndef __initialcondition_h__
#define __initialcondition_h__

dcomp initialWavefunction(double r, int n, int l);

#endif /* __initialcondition_h__ */
