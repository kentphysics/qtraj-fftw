/*
 
 eigensolver.h
 
 Copyright (c) Michael Strickland
 
 GNU General Public License (GPLv3)
 See detailed text in license directory
 
*/

#ifndef __eigensolver_h__
#define __eigensolver_h__

#include "trajectory.h"

void loadBasisStates(dcomp *basisStates, double T);

#endif /* __eigensolver_h__ */
