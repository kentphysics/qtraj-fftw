/*
 
 trajectory.h
 
 Copyright (c) Michael Strickland
 
 GNU General Public License (GPLv3)
 See detailed text in license directory
 
*/

#ifndef __trajectory_h__
#define __trajectory_h__

#include <complex>
#include <fftw3.h>
#include <vector>

#ifndef LIGHTWEIGHT
#define LIGHTWEIGHT 0
#endif

typedef std::complex<double> dcomp;

extern double L,m,dt,dx,dk,alpha,T0,Tf,t0,tmed,kappa,gam,initWidth,rMax;
extern int num,mem_size_dcomp,maxSteps,snapFreq,snapPts,potential,initType,initN,initL,initC,nmed,nTrajectories,projType;
extern int doJumps,nBasis,nThreads,derivType,temperatureEvolution,dirnameWithSeed,maxJumps,saveWavefunctions,outputSummaryFile;
extern long unsigned int randomseed;
extern dcomp *basisFunctions;
extern dcomp *wfnc,*wfncSave,*wfncInit;
extern std::ofstream outputFile;
extern std::string temperatureFile, basisFunctionsFile;
extern int cstate,lval;
extern dcomp *spaceKernel,*momKernel;
extern double firstRandomNumber;

extern double *singletOverlaps0;
extern double *singletOverlaps;
extern std::vector<std::string> metadata;

extern double *T;

const double HBARC = 0.197326938; // GeV fm

const double TC = 0.15; // Tc in GeV

const int SINGLET = 0;
const int OCTET = 1;

// potential defs
//#define POTENTIAL_COULOMB 0
//#define POTENTIAL_KMS 1

//#define POTENTIAL POTENTIAL_COULOMB
//#define POTENTIAL POTENTIAL_KMS

void initializeEvolution();
double evolveWavefunction(int nStart, int nSteps);
void evolveWavefunctionWithJumps(int nStart, double rmin);
void finalizeEvolution();
void saveNoJumpsSingletOverlaps();

#endif /* __trajectory_h__ */
