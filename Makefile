#####################################################
# qTraj Makefile 
# Author(s): Michael Strickland
#####################################################

DIR_MAIN       = ./
DIR_SRC        = $(DIR_MAIN)build/src
DIR_H          = $(DIR_MAIN)build/include
DIR_BUILD      = $(DIR_MAIN)build
DIR_OBJ        = $(DIR_BUILD)/obj

ifeq ("$(mode)", "lightweight")
MODE = 1
STATIC = 0
$(info Using lightweight mode)
else ifeq ("$(mode)", "static")
$(info Using lightweight mode - statically linked executable )
MODE = 1
STATIC = 1
else
MODE = 0
STATIC = 0
endif

HAS_MKL := $(shell ./scripts/hasMKL.sh)
DEBUG = 
OPTIMIZATION = -O2
FLOWTRACE =
LINK_OPTIONS = 
CFLAGS = $(DEBUG) $(OPTIMIZATION) $(FLOWTRACE) $(OPTIONS)
COMPILER = g++
# next line is if you want static linking
#LIBS_MKL =  -Wl,--start-group ${MKLROOT}/lib/intel64/libmkl_intel_ilp64.a ${MKLROOT}/lib/intel64/libmkl_sequential.a ${MKLROOT}/lib/intel64/libmkl_core.a -Wl,--end-group -lpthread -lm -ldl -lfftw3_threads -lfftw3 -lgsl 
LIBS_MKL = -lmkl_intel_ilp64 -lmkl_sequential -lmkl_core -lfftw3_threads -lfftw3 -lgsl -lm -ldl
LIBS_ARMA = -lfftw3_threads -lfftw3 -lgsl -lm -larmadillo
OPTIONS_MKL = -DMKL_ILP64 -m64 -std=c++11 
OPTIONS_ARMA = -std=c++11 
INCLUDES = -I $(DIR_H) 

ifeq ($(MODE),0)
ifeq ($(HAS_MKL),1)
LIBS = $(LIBS_MKL)
OPTIONS = $(OPTIONS_MKL)
C := $(shell find $(DIR_SRC) -name '*.cpp' -not -name '.*' -not -name '*_arma.cpp')
else
LIBS = $(LIBS_ARMA)
OPTIONS = $(OPTIONS_ARMA)
C := $(shell find $(DIR_SRC) -name '*.cpp' -not -name '.*' -not -name '*_mkl.cpp')
endif
else
ifeq ($(STATIC),1)
LINK_OPTIONS = -static
endif
LIBS = -lpthread -lfftw3_threads -lfftw3 -lgsl -lm 
OPTIONS = -std=c++11 -D LIGHTWEIGHT=$(MODE)
C := $(shell find $(DIR_SRC) -name '*.cpp' -not -name '.*' -not -name '*_mkl.cpp' -not -name '*_arma.cpp')
endif

C_OBJ  = $(C:$(DIR_SRC)%.cpp=$(DIR_OBJ)%.o)
OBJ = $(C_OBJ)

EXE =\
qtraj

LIB =\
libqtraj.a

$(EXE): $(OBJ)
ifeq ($(MODE),0)
ifeq ($(HAS_MKL),1)
	$(info    Eigensolver: MKL)
else
	$(info    Eigensolver: Armadillo)
endif
endif
	@echo "Linking: $@ ($(COMPILER))"
	$(COMPILER) $(LINK_OPTIONS) -o $@ $^ $(LIBS) $(INCLUDES)

$(LIB): $(OBJ)
	@echo "Generating library: $(LIB) ($(COMPILER))"
	ar -rcs $(DIR_OBJ)/$(LIB) $(OBJ)

$(DIR_OBJ)%.o: $(DIR_SRC)%.cpp
	@[ -d $(DIR_OBJ) ] || mkdir -p $(DIR_OBJ)
	@echo "Compiling: $< ($(COMPILER))"
	$(COMPILER) $(CFLAGS) $(INCLUDES) -c -o $@ $<

tests: $(LIB)
	./scripts/runTests.sh	

clean:
	@echo "Object files, library, and executable deleted"
	if [ -d "$(DIR_OBJ)" ]; then rm -rf $(EXE) $(DIR_OBJ)/*; rmdir $(DIR_OBJ); fi

.SILENT:
