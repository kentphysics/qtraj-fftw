============================================================
 qtraj-fftw 
 Version 1.0
 July 23 2021
============================================================

------------------------------------------------------------
 DOCUMENTATION
------------------------------------------------------------

Full documentation for the code and underlying physics can 
be found in the "docs" folder.

------------------------------------------------------------
 COMPILING
------------------------------------------------------------

To compile, simply type 

   make 

from the main code directory.

For full functionality, the following need to be installed 
on your sytem prior to compilation

  - FFTW 3.3.8+
  - GSL 2.6+
  - Intel MKL v2020.3.279+ _OR_ Armadillo 10.1.2+

In order to run the automatic unit tests

  - GoogleTest 1.10.0+

is required.

To run the unit tests, simply type

  make tests

in the main code directory.

** Advanced compilation **:  There are two modes for 
compiling that (1) remove the need for an eigensolver 
(MKL/Armadillo)

  make mode=lightweight

and (2) compile to static binary suitable for use on the
Open Science Grid

  make mode=static

Note that static mode is only supported on Linux machines.

------------------------------------------------------------
 USAGE
------------------------------------------------------------

There is a file "input/params.txt" that contains all 
parameters that can be adjusted at runtime.  It includes 
comments describing the various options.  To run with the 
params.txt parameters simply type

  ./qtraj

If you would like to override some parameters in the 
params.txt file from the commandline the syntax is

  ./qtraj -<PARAMNAME1> <value1> ... -<PARAMNAMEn> <valuen>

------------------------------------------------------------
 OUTPUT
------------------------------------------------------------

If the 'dirnameWithSeed' parameter is set to 1, then all 
files are output into "output-<seed>"; otherwise, they are 
output into "output".  The latter is useful for testing.  
In both cases, if the directory doesn't exist, it is 
created.  It is possible to turn on/off the output of 
the time-evolved wavefunctions and/or summary files.  The
default is to output only to "output/ratios.tsv".  If 
the file "output/ratios.tsv" already exists, then the
results are appended to this file.

------------------------------------------------------------
 LICENSE
------------------------------------------------------------

GNU General Public License (GPLv3)
See detailed text in license directory 
